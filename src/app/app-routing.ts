import { NgModule } from "@angular/core";
import { Route } from '@angular/compiler/src/core';
import { Routes,RouterModule } from '@angular/router'
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeItemComponent } from './recipes/recipe-list/recipe-item/recipe-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { ShoppingService } from './shopping-list/shopping.service';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';

const appRoutes : Routes = [
    {path:'', component: RecipesComponent},
    {path:'recipe', component: RecipesComponent ,children:[
        {path:'new', component: RecipeEditComponent},
        {path:':id', component: RecipeDetailComponent},
        {path:':id/edit', component: RecipeEditComponent}
    ]},
    {path:'shoppingList', component: ShoppingListComponent},
]


@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes,)
    ],
    exports:[RouterModule]
})
export class AppRoutingModule{}
