import { Component, OnInit } from '@angular/core';
import {Recipe} from "./recipe-list/recipe-item/recipe.model"
@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipeSelecte :Recipe;
  
  constructor() { }

  ngOnInit() {
  }

}
