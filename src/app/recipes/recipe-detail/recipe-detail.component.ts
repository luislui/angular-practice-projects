import { Component, OnInit, Input } from '@angular/core';

import { Recipe } from '../recipe.model';
import { ShoppingService } from 'src/app/shopping-list/shopping.service';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  ingredients: Ingredient[];
  id: Number ;
  constructor(private ShoppingList:ShoppingService,
              private route: ActivatedRoute,
              private recipe_data : RecipeService,
              private router: Router ,
              private recipeService : RecipeService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (data:Data)=>{
        this.id = data['id']
        console.log("detail updata")
        const ddata = this.recipe_data.getDataByID(+this.id)
        this.recipe = ddata
      }
    )
    
  }

  toShoppingList(buy_list:Recipe){
    console.log(buy_list)
    for (var i in buy_list.buy_lists){
      const a =new Ingredient(buy_list.buy_lists[i].name,buy_list.buy_lists[i].amount)
      this.ShoppingList.pushIngredients(a)
      console.log(a)
    }
    this.router.navigate(['shoppingList'])
  }  
  toEdit(){
    //this.router.navigate(['../',this.id,'edit'], {relativeTo:this.route})
    this.router.navigate(['edit',],{relativeTo:this.route})
  }

  toDel(){
    this.recipeService.del_recipe(this.id)
    this.router.navigate(['/'],{relativeTo:this.route})
  }
}
