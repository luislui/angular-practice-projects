import { Component, OnInit, Output,EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild("inputName") inputName; 
  @ViewChild("inputAmount") inputAmount; 
  @Output() Adddata = new EventEmitter<{name:String,amount:number}>()
  AddData(){
    this.Adddata.emit({name:this.inputName,amount:this.inputAmount})
  }
  constructor() { }

  ngOnInit() {
  }

}
