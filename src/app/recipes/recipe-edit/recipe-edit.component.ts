import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Route, Routes, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

import { RecipeService } from '../recipe.service';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: Number;
  editMode = false
  existan_id ;
  image_url;
  constructor(private route:ActivatedRoute, 
              private recipeService : RecipeService,
              private router : Router) { }
  new_Recipe : FormGroup
  editRecipe : Recipe
  ShoppingList = new FormArray([]);
  nameRecipe = new FormControl();
  URLRecipe = new FormControl();
  DescriptionRecipe = new FormControl();
  ngOnInit() {
    this.new_Recipe = new FormGroup({
      nameRecipe : this.nameRecipe,
      URLRecipe : this.URLRecipe,
      DescriptionRecipe : this.DescriptionRecipe,
      ShoppingList : this.ShoppingList
    })

    this.recipeService.EditRecipe.subscribe(
      (item:Recipe)=>{
        this.ShoppingList = new FormArray([]);
        console.log(item.name)
        console.log('item.buy_lists')
        if (item.buy_lists){
          item.buy_lists.forEach(item=>{
            this.ShoppingList.push(new FormGroup({
              'name' : new FormControl(item.name),
              'amount' :new FormControl(item.amount)
            }))
          })
          this.new_Recipe = new FormGroup({
            nameRecipe : new FormControl(item.name),
            URLRecipe: new FormControl(item.imagePath),
            DescriptionRecipe : new FormControl(item.description),
            ShoppingList : this.ShoppingList
          })
          this.image_url = item.imagePath
        }
        
      }
    )

    this.route.params.subscribe(
      (params:Params)=>{
        this.id = params['id']
        this.editMode = params['id'] != null;
        if (!this.editMode){
          console.log('editMode false')
        }else{
          console.log("Editing ID",params['id'] )
          this.existan_id = params['id'] 
          this.recipeService.rjgetDataByID(params['id'])
          
        }
      }
    );
  }

  Onsubmit(){
      if (this.editMode){
         console.log(this.recipeService.recipe)
      }else{
        //console.log(parse(this.recipeService.recipe.slice(-1)[0].id))
        this.recipeService.AddNewData(new Recipe (this.new_Recipe.value.nameRecipe, this.new_Recipe.value.DescriptionRecipe, this.new_Recipe.value.URLRecipe, this.recipeService.ingredToArray( this.new_Recipe.get('ShoppingList').value),this.recipeService.lastID))
        console.log(this.recipeService.recipe)
      }
  }

  addMoreList(){
    (<FormArray>this.new_Recipe.get('ShoppingList')).push(
      new FormGroup({
        'name' : new FormControl(),
         'amount' :new FormControl()
      })
    )
  }

  del_shopping(index,item){
    //console.log(item.value,index)
    //this.recipeService.del_shopping(item.value,index,this.existan_id)
    this.ShoppingList.controls.splice(index,1)
    this.ShoppingList.value.splice(index,1)
    console.log(this.new_Recipe)
  }

  updata(){
    var data = {
      name:this.new_Recipe.get('nameRecipe').value,
      url : this.new_Recipe.get('URLRecipe').value,
      descipt :this.new_Recipe.get('DescriptionRecipe').value,
      list:this.new_Recipe.get('ShoppingList').value
    }
    console.log(data)
    this.recipeService.updata(data,this.existan_id)
    this.router.navigate([`/recipe/${this.existan_id}`],{relativeTo:this.route})
  }
  
  
}
