import { Component, OnInit,EventEmitter, Output } from '@angular/core';

import{Recipe} from "./recipe-item/recipe.model"
@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('A Test Recipe1', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg'),
    new Recipe('A Test Recipe2', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg')
  ];
  @Output() recipeWasSelected = new EventEmitter<Recipe>();
  display_seleted(item){
    console.log(item)
    this.recipeWasSelected.emit(item)
  }
  constructor() { }

  ngOnInit() {
  }
  
}
