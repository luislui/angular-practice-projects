import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingService } from './shopping.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[];
  item : Ingredient
  constructor(private ShoppingService:ShoppingService) { }

  ngOnInit() {
    this.ingredients = this.ShoppingService.getIngredients();
    console.log(this.ingredients)
  }
  itemHandle(i){
    console.log(i)
    this.ShoppingService.getItemByIndex(i)
  }
}
