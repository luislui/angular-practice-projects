import { Ingredient } from '../shared/ingredient.model';

export class Recipe {
  public name: string;
  public description: string;
  public imagePath: string;
  public buy_lists: Ingredient[];
  public id:Number;

  constructor(name: string, desc: string, imagePath: string,buy_list: Ingredient[],id:Number) {
    this.name = name;
    this.description = desc;
    this.imagePath = imagePath;
    this.buy_lists =  buy_list;
    this.id = id;
  }
}
