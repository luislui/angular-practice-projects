import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

import { Recipe} from '../recipe-item/recipe.model';
@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  @Input() showrecipes: Recipe;
  @Output() showDetail = new EventEmitter<Recipe>()
  //[] = [
  //  new Recipe('A Test Recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg'),
  //  new Recipe('A Test Recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg')
  //];
  show(){
    console.log('show')
    this.showDetail.emit()
  }
  constructor() { }

  ngOnInit() {
  }

}
