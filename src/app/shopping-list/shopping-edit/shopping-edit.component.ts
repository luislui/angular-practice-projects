import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  EventEmitter,
  Output
} from '@angular/core';

import { Ingredient } from '../../shared/ingredient.model';
import { ShoppingService } from '../shopping.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { resolve } from 'url';
import { reject } from 'q';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('nameInput') nameInputRef: ElementRef;
  @ViewChild('amountInput') amountInputRef: ElementRef;
  constructor(private ShoppingService:ShoppingService) { }
  slgroup : FormGroup
  editMode : boolean = false
  ngOnInit() {
    this.slgroup = new FormGroup({
      name : new FormControl(null,Validators.required),
      amount : new FormControl(null,Validators.required,this.numberErrorHandle),
    })

    this.ShoppingService.Editing.subscribe(
      (item)=>{
        this.editMode = true
        console.log('item,',item,item['amount'])
        this.slgroup.setValue({
          name:item['name'],
          amount:item['amount']
        })
      }
    )

  }

  /*onAddItem() {
    const ingName = this.nameInputRef.nativeElement.value;
    const ingAmount = this.amountInputRef.nativeElement.value;
    const newIngredient = new Ingredient(ingName, ingAmount);
    this.ShoppingService.pushIngredients(newIngredient);
}*/
  onSubmit(e){
    if (this.editMode == false){
      console.log(this)
      const ingName = this.slgroup.value['name'];
      const ingAmount = this.slgroup.value['amount'];
      const newIngredient = new Ingredient(ingName, ingAmount);
      this.ShoppingService.pushIngredients(newIngredient);
      this.slgroup.reset()
    }else{
      
      const ingName = this.slgroup.value['name'];
      const ingAmount = this.slgroup.value['amount'];
      const newIngredient = new Ingredient(ingName, ingAmount);
      this.ShoppingService.UpdateItemByIndex(newIngredient);
      this.slgroup.reset()
      this.editMode = false
    }
  }
  numberErrorHandle(formcontrol:FormControl){
    console.log(formcontrol)
    const promise = new Promise(
      (resolve, reject) =>{
        var pattern = /^[1-9]+[0-9]*$/,
        str = '011119999';
        console.log(pattern.test(formcontrol.value))
        if (!pattern.test(formcontrol.value)){
          resolve ({numberErrorHandle:true})
        }else{
            resolve (null)
          }
        
      }
    )
    return promise
  }
  
  clearHandle(){
    this.slgroup.reset()
    this.editMode = false
  }
  deleteHandle(){
    this.ShoppingService.DeleteItemByIndex()
  }
}
