import { Ingredient } from '../shared/ingredient.model';
import { Subject } from 'rxjs';

export class ShoppingService {
  EditingredientsIndex ;
  Editing = new Subject<Ingredient>();
  ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10),
  ];
  getIngredients(){
    return this.ingredients;
  }
  pushIngredients(ingredient:Ingredient){
    this.ingredients.push(ingredient);
  }

  getItemByIndex(i){
    for (let index in this.ingredients){
      if (i ==index) {
        this.EditingredientsIndex = index
        this.Editing.next(this.ingredients[index])
      }
    }
  }

  UpdateItemByIndex(newIngredient:Ingredient){
        this.ingredients[this.EditingredientsIndex] = newIngredient
  }

  
  DeleteItemByIndex(){
    this.ingredients.splice(this.EditingredientsIndex,1)
}
  constructor() { }
}
