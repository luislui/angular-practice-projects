import { Recipe } from './recipe.model'
import { Output,EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Subject } from 'rxjs';
export class RecipeService {
  
  recipeSelect = new EventEmitter<Recipe>()
  EditRecipe = new Subject<Recipe>()
  recipe : Recipe[] = [
    new Recipe('A Test Recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',[new Ingredient('apple',4),new Ingredient("banana",6)],1),
    new Recipe('Another Test Recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',[new Ingredient('organge',4),new Ingredient("pair",6)],2)
  ];
  lastID;
    constructor(){
      
      for (let index of this.recipe){
        this.lastID = this.recipe.indexOf(index)
        this.recipe[this.lastID].id = this.lastID++
      }
   }
  getRecipe(){
    console.log(this.recipe.slice())
    console.log(this.recipe)
    return this.recipe;
  }

  getDataByID(id:Number){
    for (let i in this.recipe){
      if (this.recipe[i]['id']==id)
      {
        //this.EditRecipe.next(this.recipe[i])
        return this.recipe[i]
      }
    }
  }

  AddNewData(recipe:Recipe){
    console.log(recipe)
    
    this.recipe.push(recipe)
    this.EditRecipe.next(recipe)
  }

  rjgetDataByID(id:Number){
    for (let i in this.recipe){
      if (this.recipe[i]['id']==id)
      {
        this.EditRecipe.next(this.recipe[i])
        //return this.recipe[i]
      }
    }
  }

  ingredToArray(item){
    var data = []
    console.log('item',item)
    item.forEach(item=>{
      console.log(item)
      data.push(new Ingredient (item.name,item.amount))
    })
    return data
  }

  del_shopping(Sel_item,index,existan_id){
    this.recipe.forEach((item,index_ele )=>{
      console.log('item.buy_lists',item.buy_lists[index])
      console.log('this.ingredToArray(Sel_item)',this.ingredToArray(Sel_item)[index])
      if (this.ingredToArray(Sel_item)[index].name == item.buy_lists[index].name
          && this.ingredToArray(Sel_item)[index].amount == item.buy_lists[index].amount ){
            console.log('foundthis',this.recipe[index_ele].buy_lists)
            this.recipe[index_ele].buy_lists.splice(index,1)
      }
    })
    console.log('deled', this.recipe)
    this.rjgetDataByID(existan_id)
  }


  updata(data,existan_id){
    for (let i in this.recipe){
      if (this.recipe[i]['id']==existan_id)
      {
        this.recipe[i] = new Recipe(data.name, data.descipt,data.url, this.ingredToArray(data.list),this.recipe[i].id)
      }
    }
  }

  del_recipe(index){
    var rec = this.recipe.splice(index,1)
    console.log('rec',rec)
    this.EditRecipe.next(null)
  }
}
